﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compare2Values
{
    class Program
    {
        static void Main(string[] args)
        {
            var number1 = 3;
            var number2 = 5;

            if (number1 > number2)
            {
                Console.WriteLine($"Number 1 ({number1}) was greater than number 2 ({number2})");
            }
            else 
            {
                Console.WriteLine($"Number 1 ({number1}) was not greater than number 2 ({number2})");
            }
        }
    }
}
